# Neovim Config

# Screenshots

![#1](screenshots/1.png)
![#2](screenshots/2.png)
![#3](screenshots/3.png)
![#4](screenshots/4.png)

This is my daily vim configuration that i mostly use for python programming


I use [Vim Plug](https://github.com/junegunn/vim-plug) to install the various plugins so make sure to install it as well


# Some configuration to make all works

# Conquer Of Completion (COC)

I use [coc](https://github.com/neoclide/coc.nvim) for
- Autocompletion
- Language server
- Automatic formatting
- Linting

Here's the tool I use for the following tasks

- Linting: `pylint`
- LanguageServer: `pyls`
- Formatting: `black`

To make everything working with coc make sure to install

- coc-python
- coc-marketplace
- coc-jedi
- coc-json

make sure to also install `pyls-black`

```bash
$ pip3 install pyls-black --user
```

# Airline

## Fonts

I use [vim-airline](https://github.com/vim-airline/vim-airline) with [powerline](https://github.com/powerline/fonts.git) Along with [Fira Code Nerd Font Mono](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/FiraCode) as my font

# KEYMAPS
I have remapped some vim keys to my liking and you can find them [here](KEYMAP.md)