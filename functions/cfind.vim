function! CFIND()
    if !exists("s:highlightcursor")
        match Todo /\k*\%#\k*/
        let s:highlightcursor=1
    else
        match None
        unlet s:highlightcursor
    endif
endfunction

nnoremap <silent> <Leader>ff :call CFIND()<CR>
