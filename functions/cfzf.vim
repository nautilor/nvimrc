function! CFZF()
    let keyword = input(':')
    :execute 'vimgrep /' . keyword . '/g ./**/*.py'
    :execute 'cw'
endfunction

nnoremap <C-o> :call CFZF()<CR>
