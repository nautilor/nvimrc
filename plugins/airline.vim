let g:airline_powerline_fonts = 1
set t_Co=256
let g:airline_theme="violet"
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_close_button = 0
let g:airline#extensions#tabline#show_splits = 0
let airline#extensions#tabline#show_buffers = 0
