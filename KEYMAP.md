# Standard vim

> NOTE: I have remapped the `Leader` to the `Spacebar`

## Normal Mode
```
Ctrl + k => go to the next tab
Ctrl + j => go to the previous tab
gf       => open the highlighted file in a new tab
gd       => jump to tag
dg       => jump back from tag
```

## Insert Mode
```
"       => types '""'
'       => types '''
(       => types '()'
[       => types '[]'
{       => types '{}'
{<CR>}  => types '{}' but with an empty line in the middle
{;<CR>} => types '{}' but with an empty line in the middle and ';' at the end
```


# Plugins

## Normal Mode
```
Ctrl + m    => toggle comment on the line
Ctrl + n    => toggle NERDTree
<Leader> / gf  => open NERDTree to the file path
<Leader> / rr  => Semshi rename
Tab         => Semshi goto next instance
<Leader> / Tab => Semshi go to prev instance
<Leader> / f   => go to next function
<Leader> / F   => go to previous function
<Leader> / ee  => show current error
<Leader> / ge  => goto to current error
<Leader> / \   => replace text with emoji
```

# Functions

## Normal Mode

```
Ctrl + o   => find files containing text
<Leader> / ff => toogle highlight word to sho cursor position 
```

# Other Default Keybinds that I use

# Plugins

## Normal Mode
```
Ctrl + p => Fuzzy finder for file
```
