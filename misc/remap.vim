nnoremap <C-k> gt
nnoremap <C-j> gT
nnoremap gf <C-W>gf
nnoremap gd <C-]>
nnoremap dg <C-t>

inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O
