:hi TabLineSel ctermfg=black ctermbg=blue
:hi TabLine ctermfg=black ctermbg=6
:hi TabLineFill ctermfg=6 ctermbg=6
set fillchars+=vert:\┃
highlight VertSplit cterm=NONE ctermfg=Blue ctermbg=NONE
highlight SignColumn guibg=NONE ctermbg=NONE
highlight EndOfBuffer ctermfg=black ctermbg=black

