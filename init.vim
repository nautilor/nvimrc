:let mapleader = "\<Space>"

source $HOME/.config/nvim/misc/theme.vim
source $HOME/.config/nvim/misc/remap.vim
source $HOME/.config/nvim/misc/generic.vim

source $HOME/.config/nvim/plugins/installed.vim
source $HOME/.config/nvim/plugins/gitgutter.vim
source $HOME/.config/nvim/plugins/nerdtree.vim
source $HOME/.config/nvim/plugins/nerdcommenter.vim
source $HOME/.config/nvim/plugins/airline.vim
source $HOME/.config/nvim/plugins/semshi.vim

source $HOME/.config/nvim/functions/cfzf.vim
source $HOME/.config/nvim/functions/cfind.vim

